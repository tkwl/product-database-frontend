import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, Validators } from '@angular/forms';
import { Product } from '../core/models/product.interface';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SNACKBAR_MESSAGES } from '../core/utils/snackbars';

@Component({
  selector: 'app-edit-product-dialog',
  templateUrl: './edit-product-dialog.component.html',
})
export class EditProductDialogComponent {
  editProductForm = this.fb.group({
    id: [''],
    name: ['', Validators.required],
    preis: ['', Validators.required],
  });
  isLoading = false;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<EditProductDialogComponent>,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    const product = { ...data.product };
    this.editProductForm.patchValue(product);
  }

  onSubmit() {
    if (this.editProductForm.valid) {
      this.isLoading = true;
      this.dialogRef.close(this.editProductForm.value);
    } else {
      this.snackBar.open(SNACKBAR_MESSAGES.formInvalid, 'Close', {
        duration: 3000,
      });
    }
  }

  close() {
    this.isLoading = false;
    this.dialogRef.close();
  }
}
