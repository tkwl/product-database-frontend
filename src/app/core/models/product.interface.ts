export interface Product {
  id: number;
  name: string;
  artikelnummer: string;
  preis: number;
}
