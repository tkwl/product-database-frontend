import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, catchError, throwError } from 'rxjs';
import { Product } from '../../models/product.interface';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private apiUrl = 'http://localhost:8080';
  constructor(private http: HttpClient) {}

  getAllProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.apiUrl}/products`);
  }

  addProduct(product: { name: string; preis: number }): Observable<Product> {
    return this.http.post<Product>(`${this.apiUrl}/products`, product).pipe(
      catchError((error) => {
        return throwError(() => error);
      })
    );
  }

  deleteProducts(productIds: number[]): Observable<any> {
    return this.http
      .request('delete', `${this.apiUrl}/products`, {
        body: productIds,
      })
      .pipe(
        catchError((error) => {
          return throwError(() => error);
        })
      );
  }

  updateProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(
      `${this.apiUrl}/products/${product.id}`,
      product
    );
  }
}
