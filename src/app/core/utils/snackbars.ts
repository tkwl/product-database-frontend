export const SNACKBAR_MESSAGES = {
  fetchError: 'Error fetching products',
  deleteError: 'Error deleting products',
  notFoundError: 'Product not found',
  updateError: 'Error updating product',
  deleteSuccess: 'Deletion successful',
  updateSuccess: 'Update successful',
  formInvalid: 'Please fill in all fields',
  productAdded: 'Product addition successful',
  genericError: 'Something went wrong. Please try again',
};
