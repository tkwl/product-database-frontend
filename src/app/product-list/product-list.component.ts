import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { Product } from '../core/models/product.interface';
import { ProductService } from '../core/services/product/product.service';
import { catchError } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AddProductDialogComponent } from '../add-product-dialog/add-product-dialog.component';
import { EditProductDialogComponent } from '../edit-product-dialog/edit-product-dialog.component';
import { SNACKBAR_MESSAGES } from '../core/utils/snackbars';

const SNACKBAR_DURATION = 3000;

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
})
export class ProductListComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = [
    'select',
    'id',
    'artikelnummer',
    'name',
    'preis',
    'edit',
  ];

  products: Product[] = [];
  selection = new SelectionModel<Product>(true, []);
  isLoading: boolean = false;
  private subscriptions = new Subscription();

  constructor(
    private productService: ProductService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.subscriptions.add(
      this.productService
        .getAllProducts()
        .pipe(
          catchError((err) => {
            this.handleError(SNACKBAR_MESSAGES.fetchError);
            return of([]);
          })
        )
        .subscribe((products) => (this.products = products))
    );
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  handleError(message: string): void {
    this.isLoading = false;
    this.snackBar.open(message, 'Close', {
      duration: SNACKBAR_DURATION,
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.products.length;
    return numSelected === numRows;
  }

  masterToggle() {
    if (this.isLoading) return;
    this.isAllSelected()
      ? this.selection.clear()
      : this.products.forEach((row) => this.selection.select(row));
  }

  deleteSelected() {
    const selectedIds = this.selection.selected.map((product) => product.id);
    this.isLoading = true;
    this.selection.clear();
    this.subscriptions.add(
      this.productService.deleteProducts(selectedIds).subscribe({
        next: (response) => {
          this.products = this.products.filter(
            (product) => !selectedIds.includes(product.id)
          );
          this.isLoading = false;
          this.snackBar.open(SNACKBAR_MESSAGES.deleteSuccess, 'Close', {
            duration: SNACKBAR_DURATION,
          });
        },
        error: () => {
          this.handleError(SNACKBAR_MESSAGES.deleteError);
        },
      })
    );
  }

  openAddProductDialog(): void {
    const dialogRef = this.dialog.open(AddProductDialogComponent);

    this.subscriptions.add(
      dialogRef.afterClosed().subscribe((newProduct) => {
        if (newProduct) {
          this.products = [...this.products, newProduct];
        }
      })
    );
  }

  openEditProductDialog(product: Product) {
    const dialogRef = this.dialog.open(EditProductDialogComponent, {
      data: { product },
    });

    this.subscriptions.add(
      dialogRef.afterClosed().subscribe((updatedProduct: Product) => {
        if (updatedProduct) {
          this.updateProduct(updatedProduct);
        }
      })
    );
  }

  updateProduct(updatedProduct: Product) {
    this.isLoading = true;
    this.productService.updateProduct(updatedProduct).subscribe(
      (product: Product) => {
        const index = this.products.findIndex((p) => p.id === product.id);
        if (index !== -1) {
          this.products = [
            ...this.products.slice(0, index),
            product,
            ...this.products.slice(index + 1),
          ];
          this.snackBar.open(SNACKBAR_MESSAGES.updateSuccess, 'Close', {
            duration: SNACKBAR_DURATION,
          });
        } else {
          this.handleError(SNACKBAR_MESSAGES.notFoundError);
        }
        this.isLoading = false;
      },
      (error) => {
        this.isLoading = false;
        this.handleError(SNACKBAR_MESSAGES.updateError);
      }
    );
  }
}
