import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '../core/services/product/product.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef } from '@angular/material/dialog';
import { SNACKBAR_MESSAGES } from '../core/utils/snackbars';
const productFormValidators = {
  name: ['', Validators.required],
  preis: ['', Validators.required],
};

const SNACKBAR_DURATION = 3000;

@Component({
  selector: 'app-add-product-dialog',
  templateUrl: './add-product-dialog.component.html',
  styleUrls: ['./add-product-dialog.component.css'],
})
export class AddProductDialogComponent {
  addProductForm: FormGroup;
  isLoading: boolean = false;

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<AddProductDialogComponent>
  ) {
    this.initializeForm();
  }

  initializeForm() {
    this.addProductForm = this.fb.group(productFormValidators);
  }

  onSubmit() {
    if (this.addProductForm.valid) {
      this.addProduct();
    } else {
      this.snackBar.open(SNACKBAR_MESSAGES.formInvalid, 'Close', {
        duration: SNACKBAR_DURATION,
      });
    }
  }

  private addProduct() {
    this.isLoading = true;
    this.addProductForm.disable();

    this.productService.addProduct(this.addProductForm.value).subscribe({
      next: (product) => {
        this.isLoading = false;
        this.addProductForm.enable();
        this.snackBar.open(SNACKBAR_MESSAGES.productAdded, 'Close', {
          duration: SNACKBAR_DURATION,
        });
        this.dialogRef.close(product);
      },
      error: (err) => {
        this.isLoading = false;
        this.addProductForm.enable();
        this.snackBar.open(
          err.message || SNACKBAR_MESSAGES.genericError,
          'Close',
          {
            duration: SNACKBAR_DURATION,
          }
        );
      },
    });
  }
}
